var hideCompletedTasks = 0;
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: false,
        cache: false
    });

    if(readCookie('hideCompletedTasks') != null){
        $("#hideCompletedTasks").prop("checked", true);
        hideCompletedTasks = 1;
    }

    loadTasks();

    $("#newTask").click(function () {
        $('#formTaskModal').modal('show');

        $("#saveBtn").click(function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: siteUrl + "/task/create",
                data: {title: $("#title").val(), description: $("#description").val()},
                dataType: 'json',
                success: function (data) {
                    if (typeof data.errors != "undefined") {
                        var error = "";
                        $.each(data.errors, function (key, item) {
                            error += item[0];
                        });
                        alert(error);
                        return false;
                    }

                    hideModal();
                    loadTasks();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });

    $("#search").keyup(function( event ) {
        loadTasks();
    });

    $("#clearFilter").click(function () {
       $("#search").val("");
       loadTasks();
    });

    $("#hideCompletedTasks").change(function () {
        if($(this).is(":checked")) {
            setCookie('hideCompletedTasks',1, 365);
            hideCompletedTasks = 1;
        } else {
            setCookie('hideCompletedTasks',0, 0);
            hideCompletedTasks = 0;
        }
        loadTasks();
    });

});

function loadTasks() {
    var filterText = $("#search").val();
    $.ajax({
        type: 'GET',
        url: siteUrl + "/tasks/list",
        data: {filter: filterText, hideCompletedTasks: hideCompletedTasks},
        dataType: 'json',
        success: function (data) {
            if (typeof data.errors != "undefined") {
                var error = "";
                $.each(data.errors, function (key, item) {
                    error += item[0];
                });
                alert(error);
                return false;
            }

            if (data.status == 'success') {
                $("#tasklist").html(data.html);
                resetButtonAction();
            }
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function resetButtonAction(){
    $(".delBtn").each(function () {
        $(this).click(function () {
            var r = confirm("Biztos törlöd a feladatot?");
            if (r == true) {
                $.ajax({
                    type: 'DELETE',
                    url: siteUrl + "/task/" + $(this).val(),
                    dataType: 'json',
                    success: function (data) {
                        if (typeof data.errors != "undefined") {
                            var error = "";
                            $.each(data.errors, function (key, item) {
                                error += item[0];
                            });
                            alert(error);
                            return false;
                        }

                        loadTasks();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
    $(".doneBtn").each(function () {
        $(this).click(function () {
            var r = confirm("Biztos befejezed a feladatot?");
            if (r == true) {
                $.ajax({
                    type: 'GET',
                    url: siteUrl + "/task/done/" + $(this).val(),
                    dataType: 'json',
                    success: function (data) {
                        if (typeof data.errors != "undefined") {
                            var error = "";
                            $.each(data.errors, function (key, item) {
                                error += item[0];
                            });
                            alert(error);
                            return false;
                        }

                        loadTasks();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
    $(".editBtn").each(function () {
        $(this).click(function () {
            var taskID = $(this).val();
            $.ajax({
                type: 'GET',
                url: siteUrl + "/task/"+taskID,
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (typeof data.errors != "undefined") {
                        var error = "";
                        $.each(data.errors, function (key, item) {
                            error += item[0];
                        });
                        alert(error);
                        return false;
                    }

                    $('#formTaskModal').modal('show');
                    $("#title").val(data.title);
                    $("#description").val(data.description);

                    $("#saveBtn").click(function (e) {
                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: siteUrl + "/task/update/"+taskID,
                            data: {title: $("#title").val(), description: $("#description").val()},
                            dataType: 'json',
                            success: function (data) {
                                if (typeof data.errors != "undefined") {
                                    var error = "";
                                    $.each(data.errors, function (key, item) {
                                        error += item[0];
                                    });
                                    alert(error);
                                    return false;
                                }

                                hideModal();
                                loadTasks();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
                    });

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
}

function hideModal(){
    $("#saveBtn").unbind( "click" );
    $("#title").val("");
    $("#description").val("");
    $('#formTaskModal').modal('hide');
}

function setCookie(name, value, expirationInDays) {
    var date = new Date();
    date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
    document.cookie = name + '=' + value + '; ' + 'expires=' + date.toUTCString() +';path=/';
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

