@if($tasks->count())
@foreach($tasks as $task)
    <tr
        @if($task->completed)
         class="success"
        @endif
    >
        <td>{{$task->id}}</td>
        <td>{{$task->title}}</td>
        <td>{{$task->description}}</td>
        <td>{{$task->timeAgo}}</td>
        <td>{{$task->completed ? "Igen" : "Nem"}}</td>
        <td>{{$task->completed_at}}</td>
        <td class="text-right">
            @if(!$task->completed)
            <button type="button" class="btn btn-success doneBtn" value="{{ $task->id }}">Kész</button>
            <button type="button" class="btn btn-warning editBtn" value="{{ $task->id }}">Szerkeszt</button>
            @endif
            <button type="button" class="btn btn-danger delBtn" value="{{ $task->id }}">Töröl</button>
        </td>
    </tr>
@endforeach
@else
    <tr>
        <td colspan="5">Nincs feladatod.</td>
    </tr>
@endif