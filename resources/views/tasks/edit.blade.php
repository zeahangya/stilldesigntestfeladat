<div class="modal fade" tabindex="-1" role="dialog" id="editTaskModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Feladat szerkesztése</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Megnevezés</label>
                    <input type="text" class="form-control" id="title" placeholder="Feladat címe">
                </div>
                <div class="form-group">
                    <label for="description">Leírás</label>
                    <textarea class="form-control" rows="3" id="description"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Mégse</button>
                <button type="button" class="btn btn-primary" id="saveBtn">Mentés</button>
            </div>
        </div>
    </div>
</div>