@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6"><h3 class="pull-left">Feladataim</h3></div>
                            <div class="col-md-6 text-right">
                                <div class="form-group form-inline">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="hideCompletedTasks"> Befejezett feladatok elrejtése
                                        </label>
                                    </div>
                                    <input type="text" name="search" id="search" placeholder="Feladatok szűrése" class="form-control" />
                                    <button id="clearFilter">Alapállapot</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if ($tasks->count())
                            <table class="table table-striped">
                                <thead>
                                 <tr>
                                    <td>#</td>
                                    <td>Megnevezés</td>
                                    <td>Leírás</td>
                                    <td>Létrehozva</td>
                                    <td>Lezárt</td>
                                    <td>Befejezve</td>
                                    <td class="text-center">Műveletek</td>
                                 </tr>
                                </thead>
                                <tbody id="tasklist">

                                </tbody>
                            </table>
                        @else
                            <p>Nincs feladatod még.</p>
                        @endif

                            <p><button class="btn btn-primary" type="submit" id="newTask">Új feladat</button></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('tasks.create')
@endsection