<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'title' => 'Feladat 1',
            'description' => 'Feladat 1 leírása',
            'user_id' => 1,
            'completed' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'completed_at' => \Carbon\Carbon::now()
        ]);
        DB::table('tasks')->insert([
            'title' => 'Feladat 2',
            'description' => 'Feladat 2 leírása',
            'user_id' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
