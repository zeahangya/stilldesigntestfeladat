<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Task;
use App\User;
use Carbon\Carbon;

class TaskController extends Controller
{
    private $messages = [
        'title.required' => 'A megnevezés megadása kötelező.',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = auth()->user()->tasks()->get();

        return view('tasks.index', compact('tasks'));
    }

    /**
     * Create new task
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $task =  new Task;
        $task->title = $request->input('title');
        $task->user_id = auth()->user()->id;
        $task->description = ($request->input('description') !== "") ? $request->input('description'): "";
        $task->save();

        $result = [
            'status' => 'success',
            'message' => 'Feladat sikeresen létrehozva.'
        ];

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Delete user's task
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        $task = Task::where('user_id', auth()->user()->id)->where('id',$id)->first();

        if(is_null($task)){
            return response()->json(['errors'=> ['Nem létező feladat.']]);
        }

        $task->delete();

        $result = [
            'status' => 'success',
            'message' => 'Feladat sikeresen törölve.'
        ];

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * List user's tasks
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function listTask(Request $request){

        $tasks = auth()->user()->tasks();

        if($request->input('filter')){
            $tasks = $tasks->where(function($q) use ($request) {
                $q->where('title', 'like', '%'.$request->input('filter').'%')
                    ->orWhere('description','like','%'.$request->input('filter').'%');
            });
        }

        if($request->input('hideCompletedTasks') && $request->input('hideCompletedTasks') == 1){
            $tasks = $tasks->where('completed',0);
        }

        $tasks = $tasks->orderBy('completed', 'ASC')->orderBy('completed_at', 'DESC')->orderBy('created_at','DESC')->get();

        $returnHTML = view('tasks.list',compact('tasks'))->render();
        return response()->json( array('status' => 'success', 'html'=>$returnHTML) );

    }

    /**
     * Finish user's task
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function completeTask($id){
        $task = Task::where('user_id', auth()->user()->id)->where('id',$id)->first();

        if(is_null($task)){
            return response()->json(['errors'=> ['Nem létező feladat.']]);
        }

        $task->completed = 1;
        $task->completed_at = Carbon::now();
        $task->save();

        $result = [
            'status' => 'success',
            'message' => 'Feladat sikeresen befejezve.'
        ];

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Get user's task by id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getTask($id){
        $task = Task::where('user_id', auth()->user()->id)->where('id',$id)->first();

        if(is_null($task)){
            return response()->json(['errors'=> ['Nem létező feladat.']]);
        }

        $result = [
            'status' => 'success',
            'title' => $task->title,
            'description' => $task->description
        ];

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Update user's task
     *
     * @param int $id
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function updateTask($id, Request $request){
        $task = Task::where('user_id', auth()->user()->id)->where('id',$id)->first();

        if(is_null($task)){
            return response()->json(['errors'=> ['Nem létező feladat.']]);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $task->title = $request->input('title');
        $task->description = ($request->input('description') !== "") ? $request->input('description'): "";
        $task->save();

        $result = [
            'status' => 'success',
            'message' => 'Feladat sikeresen módosítva.'
        ];

        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
