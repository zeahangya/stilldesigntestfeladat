<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'TaskController@index');
Route::get('/tasks', 'TaskController@index')->name('tasks');
Route::get('/tasks/list', 'TaskController@listTask');
Route::post('/task/create', 'TaskController@store');
Route::delete('/task/{id}', 'TaskController@destroy');
Route::get('/task/{id}', 'TaskController@getTask');
Route::get('/task/done/{id}', 'TaskController@completeTask');
Route::post('/task/update/{id}', 'TaskController@updateTask');
